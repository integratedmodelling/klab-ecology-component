package org.integratedmodelling.ecology.biomass.lpjguess;

import org.integratedmodelling.exceptions.KlabRuntimeException;

public enum HydrologyType {

    RAINFED,
    IRRIGATED;

    public static HydrologyType get(String s) {

        switch (s.toUpperCase()) {
        case "RAINFED":
            return RAINFED;
        case "IRRIGATED":
            return IRRIGATED;
        }

        throw new KlabRuntimeException("unknown irrigation type: " + s);
    }
}
