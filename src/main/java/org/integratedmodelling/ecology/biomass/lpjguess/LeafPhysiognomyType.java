package org.integratedmodelling.ecology.biomass.lpjguess;

import org.integratedmodelling.exceptions.KlabRuntimeException;

public enum LeafPhysiognomyType {

    NOLEAFTYPE,
    NEEDLELEAF,
    BROADLEAF;

    public static LeafPhysiognomyType get(String s) {
        if (s.equalsIgnoreCase("BROADLEAF")) {
            return BROADLEAF;
        } else if (s.equalsIgnoreCase("NEEDLELEAF")) {
            return NEEDLELEAF;
        } else if (s.equalsIgnoreCase("NOLEAFTYPE")) {
            return NOLEAFTYPE;
        }

        throw new KlabRuntimeException("unknown leaf physiognomy: " + s);

    }
}
