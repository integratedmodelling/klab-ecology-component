package org.integratedmodelling.ecology.biomass.lpjguess;

import org.integratedmodelling.exceptions.KlabRuntimeException;

public enum NaturalVeg {

    NONE,
    GRASSONLY,
    ALL;

    static public NaturalVeg get(String s) {
        switch (s.toUpperCase()) {
        case "NONE":
            return NONE;
        case "GRASSONLY":
            return GRASSONLY;
        case "ALL":
            return ALL;
        }

        throw new KlabRuntimeException("unknown natural vegetation statement: " + s);
    }
}
