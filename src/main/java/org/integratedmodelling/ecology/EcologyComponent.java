package org.integratedmodelling.ecology;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.components.Component;
import org.integratedmodelling.api.components.Initialize;
import org.integratedmodelling.api.components.Setup;
import org.integratedmodelling.common.vocabulary.EcologyNS;

/**
 * Do-nothing example of a component declaration. Use the
 * {@link org.integratedmodelling.api.plugin.Component} annotation to declare
 * it. It is mandatory to provide the class with at least the component ID and
 * the version, although it doesn't need to have any methods.
 *
 * This class does not provide any services: all the component API is
 * implemented using contextualizers tagged with the appropriate @Prototype
 * annotation. These should be in the same package or a subpackage of the
 * component's. Each prototype will turn into a function in the client, calling
 * the local code if local or a remote service if made available from a
 * networked node.
 *
 * @author ferdinando.villa
 *
 */
@Component(
        id = "im.ecology",
        version = Version.CURRENT)
public class EcologyComponent {

    /**
     * The method annotated with
     * {@link org.integratedmodelling.api.plugin.Setup}, if present, must return
     * a boolean and may throw exceptions. It is called explicitly from a client
     * by an administrator, and is used for one-time setup of the component
     * (e.g. to populate a database). It is optional; the one here does nothing
     * and could (should) be omitted in a real-life situation.
     *
     * @return true
     */
    @Setup(asynchronous = false)
    public boolean setup() {
        return true;
    }

    /**
     * The method annotated with
     * {@link org.integratedmodelling.api.plugin.Initialize}, if present, must
     * return a boolean and may throw exceptions. It is called every time the
     * component is registered, which happens at server startup, and is used to
     * ensure that the component can operate (e.g. all data it needs are there,
     * setup has been done correctly etc.). If it returns true, the component
     * will be advertised to all clients that have access rights to it, and
     * become available as a function prototype in their language API. It is
     * also optional; if not present, the component is assumed operational.
     *
     * @return true
     */

    @Initialize
    public boolean initialize() {
        // Synchronise with the Ecological namespace stuff (so we can easily get
        // States etc)
        EcologyNS.synchronize();

        return true;
    }
}
