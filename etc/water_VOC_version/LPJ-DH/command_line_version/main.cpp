///////////////////////////////////////////////////////////////////////////////////////
/// \file main.cpp
/// \brief Main module for command line version of LPJ-GUESS
///
/// \author Ben Smith
/// $Date: 2012-02-15 09:52:12 +0100 (Wed, 15 Feb 2012) $
///
///////////////////////////////////////////////////////////////////////////////////////

#include "config.h"
#include "guess.h"
#include "framework.h"


///////////////////////////////////////////////////////////////////////////////////////
// LOG FILE
// The name of the log file to which output from all dprintf and fail calls is sent is
// set here

xtring file_log="guess.log";



///////////////////////////////////////////////////////////////////////////////////////
// MAIN
// This is the function called when the executable is run

int main(int argc,char* argv[]) {

	// Set our shell for the model to communicate with the world
	set_shell(new CommandLineShell(file_log));

	// Call the framework
	framework(argc,argv);

	// Say goodbye
	dprintf("\nFinished\n");

	return 0;
}
