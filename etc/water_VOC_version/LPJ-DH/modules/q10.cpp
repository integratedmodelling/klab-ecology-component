///////////////////////////////////////////////////////////////////////////////////////
/// \file q10.h
/// \brief Q10 calculations for photosynthesis
///
/// Definitions of the LookupQ10 objects.
///
/// $Date: 2011-08-10 15:15:49 +0200 (Wed, 10 Aug 2011) $
///
///////////////////////////////////////////////////////////////////////////////////////

#include "config.h"
#include "q10.h"

LookupQ10 lookup_tau(0.57, 2600.0);
LookupQ10 lookup_ko(1.2, 3.0e4);
LookupQ10 lookup_kc(2.1, 30.0);
