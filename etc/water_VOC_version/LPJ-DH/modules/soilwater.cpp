///////////////////////////////////////////////////////////////////////////////////////
/// \file soilwater.cpp
/// \brief Soil hydrology and snow
///
/// Version including evaporation from soil surface, based on work by Dieter Gerten,
/// Sibyll Schaphoff and Wolfgang Lucht, Potsdam
///
/// Includes baseflow runoff
///
/// \author Ben Smith
/// $Date: 2011-12-08 12:57:43 +0100 (Thu, 08 Dec 2011) $
///
///////////////////////////////////////////////////////////////////////////////////////

// WHAT SHOULD THIS FILE CONTAIN?
// Module source code files should contain, in this order:
//   (1) a "#include" directive naming the framework header file. The framework header
//       file should define all classes used as arguments to functions in the present
//       module. It may also include declarations of global functions, constants and
//       types, accessible throughout the model code;
//   (2) other #includes, including header files for other modules accessed by the
//       present one;
//   (3) type definitions, constants and file scope global variables for use within
//       the present module only;
//   (4) declarations of functions defined in this file, if needed;
//   (5) definitions of all functions. Functions that are to be accessible to other
//       modules or to the calling framework should be declared in the module header
//       file.
//
// PORTING MODULES BETWEEN FRAMEWORKS:
// Modules should be structured so as to be fully portable between models (frameworks).
// When porting between frameworks, the only change required should normally be in the
// "#include" directive referring to the framework header file.

#include "config.h"
#include "soilwater.h"

void snow(double prec, double temp, double& snowpack, double& rain_melt) {

	// Daily calculation of snowfall and rainfall from precipitation and snow melt from
	// snow pack; update of snow pack with new snow and snow melt and snow melt

	// INPUT PARAMETERS
	// prec = precipitation today (mm)
	// temp = air temperature today (deg C)

	// INPUT AND OUTPUT PARAMETER
	// snowpack = stored snow (rainfall mm equivalents)

	// OUTPUT PARAMETERS
	// rain_melt = rainfall and snow melt today (mm)

	const double TSNOW = 0.0;
		// maximum temperature for precipitation as snow (deg C)
		// previously 2 deg C; new value suggested by Dieter Gerten 2002-12
	const double SNOWPACK_MAX = 10000.0;
		// maximum size of snowpack (mm) (S. Sitch, pers. comm. 2001-11-28)

	double melt;
	if (temp < TSNOW) {						// snowing today
		melt = -min(prec, SNOWPACK_MAX-snowpack);
	} else {								// raining today
		// New snow melt formulation
		// Dieter Gerten 021121
		// Ref: Choudhury et al 1998
		melt = min((1.5+0.007*prec)*(temp-TSNOW), snowpack);
	}
	snowpack -= melt;
	rain_melt = prec + melt;
	//if(rain_melt>0){
	 //dprintf("check the values for prec and melt");
	//}
}

void hydrology_lpjf(Patch& patch, Climate& climate, double rain_melt, double wcont_prev[NSOILLAYER], double rain_melt_prev, double wcont_evap_prev, double perc_base,
		double perc_exp, double awc[NSOILLAYER], double fevap, double snowpack,
		bool percolate, double max_rain_melt, double awcont[NSOILLAYER],
		double wcont[NSOILLAYER], double& wcont_evap, double& runoff, double& runoff_surf_1, double& runoff_drain_1, double& runoff_base_1) {

	// Daily update of water content for each soil layer given snow melt, rainfall,
	// evapotranspiration from vegetation (AET) and percolation between layers;
	// calculation of runoff

	// INPUT PARAMETERS
	// rain_melt  = inward water flux to soil today (rain + snowmelt) (mm)
	// perc_base  = coefficient in percolation calculation (K in Eqn 31, Haxeltine
	//              & Prentice 1996) K is an emprical parameter in mm dependent on soil texture
	// perc_exp   = exponent in percolation calculation (=4 in Eqn 31, Haxeltine &
	//              Prentice 1996)
	// awc        = array containing available water holding capacity of soil
	//              layers (mm rainfall) [0=upper layer]
	// fevap      = fraction of modelled area (grid cell or patch) subject to
	//              evaporation from soil surface
	// snowpack   = depth of snow (mm)

	// INPUT AND OUTPUT PARAMETERS
	// wcont      = array containing water content of soil layers [0=upper layer] as
	//              fraction of available water holding capacity (AWC)
	// wcont_evap = water content of evaporation sublayer at top of upper soil layer
	//              as fraction of available water holding capacity (AWC)
	// awcont     = wcont averaged over the growing season - guess2008


	// OUTPUT PARAMETER
	// runoff     = total daily runoff from all soil layers (mm/day)
   
	const double SOILDEPTH_EVAP = 200.0;
		// depth of sublayer at top of upper soil layer, from which evaporation is
		// possible (NB: must not exceed value of global constant SOILDEPTH_UPPER)
	const double BASEFLOW_FRAC = 0.5;
		// Fraction of standard percolation amount from lower soil layer that is
		// diverted to baseflow runoff
	const double K_DEPTH = 0.4;
	const double K_AET = 0.52;
		// Fraction of total (vegetation) AET from upper soil layer that is derived
		// from the top K_DEPTH (fraction) of the upper soil layer
		// (parameters for calculating K_AET_DEPTH below)
	const double K_AET_DEPTH = (SOILDEPTH_UPPER/SOILDEPTH_EVAP-1.0)*
								(K_AET/K_DEPTH-1.0)/(1.0/K_DEPTH-1.0)+1.0;
		// Weighting coefficient for AET flux from evaporation layer, assuming active
		//   root density decreases with soil depth
		// Equates to 1.3 given SOILDEPTH_EVAP=200 mm, SOILDEPTH_UPPER=500 mm,
		//   K_DEPTH=0.4, K_AET=0.52

	int s;
	double aet;				// AET for a particular layer and individual (mm)
	double aet_layer[NSOILLAYER]; // total AET for each soil layer (mm)
	double perc_frac;


	for (s=0; s<NSOILLAYER; s++) {
		aet_layer[s] = 0.0;
	}

	patch.aet_tot=0.0;
	patch.evap=0.0;
	
	//double aet_total = 0.0;
	//double add_water=0.0;
	//double wcont_evap_prev;

	// Sum AET(actural evaportransportion) for across all vegetation individuals
	Vegetation& vegetation=patch.vegetation;
	vegetation.firstobj();
	while (vegetation.isobj) {
		Individual& indiv = vegetation.getobj();

		for (s=0; s<NSOILLAYER; s++) {
			aet = patch.pft[indiv.pft.id].fuptake[s] * indiv.aet;
			aet_layer[s] += aet;
			patch.aet_tot += aet;
		}
		vegetation.nextobj();
	}

	// Evaporation from soil surface

	// guess2008 - changed to wcont_evap**2, as in LPJ-mL
	// - see Bondeau et al. (2007),  Rost et al. (2008)
	// Added the snowdepth restriction too.
	//double evap = 0.0;
	if (snowpack < 10.0) {					// evap only if snow depth < 10mm
		patch.evap = climate.eet * PRIESTLEY_TAYLOR * wcont_evap * wcont_evap * fevap;
	}
	
	// Implement in- and outgoing fluxes to upper soil layer
	// BLARP: water content can become negative, though apparently only very slightly
	//    - quick fix implemented here, should be done better later
	
/*	if(date.year==0 && date.day==0)
		patch.dadd_water=0;
	else 
		patch.dadd_water=patch.stand.gridcell.dsurf_ave;*/
	
	//if (patch.dadd_water>0)  //jing add it
	   //dprintf("\nI arrived here, check the values\n");

	//wcont_prev[0] = wcont[0];  //put previous day water content into this variable. to get the change of water content
	wcont_prev[1] = wcont[1];
	wcont[0] += (rain_melt - aet_layer[0] - patch.evap) / awc[0];  //put the additional water in the initial_infiltration function. 
//   if (wcont[0]>0)
	//   dprintf("\n water content bigger than 0");
	//if (date.month==9||date.month==10||date.month==11)
		//dprintf("\n water content for day",date.day,wcont[0]);

	if (wcont[0] != 0.0 && wcont[0] < 0.0001) { // guess2008 - bugfix
		wcont[0] = 0.0;
	}

	// Surface runoff
	double runoff_surf = 0.0;
	// double TWI
	//double topo=(1-0.5)/(constant range(TWI))*TWI +1;
	double topo= 1-patch.stand.gridcell.slope/90;   //assuming lower threshold value for steep places(lower TWI points) it should be a function of TWI
    
	if (wcont[0] > topo) {
		runoff_surf = (wcont[0]-topo) * awc[0];
		wcont[0] = topo;
	}
	

	// Update water content in evaporation layer for tomorrow
    //wcont_evap_prev=wcont_evap;
	 wcont_evap += (rain_melt-aet_layer[0]*SOILDEPTH_EVAP*K_AET_DEPTH/SOILDEPTH_UPPER-patch.evap)/awc[0];


	if (wcont_evap > wcont[0]) {
		wcont_evap = wcont[0];
	}

	// Percolation from evaporation layer
	double perc = 0.0;
	if (percolate) {
		perc = min(SOILDEPTH_EVAP/SOILDEPTH_UPPER*perc_base*pow(wcont_evap,perc_exp),
													max_rain_melt);
	}
	wcont_evap -= perc/awc[0];

	
	

	// Percolation and fluxes to and from lower soil layer(s)

	// Transfer percolation between soil layers
	// Excess water transferred to runoff
	// Eqns 26, 27, 31, Haxeltine & Prentice 1996

	double runoff_drain = 0.0;

	for (s=1; s<NSOILLAYER; s++) {

		// Percolation
		// Allow only on days with rain or snowmelt (Dieter Gerten, 021216)

		if (percolate) {
			perc = min(perc_base*pow(wcont[s-1],perc_exp), max_rain_melt);
		} else {
			perc=0.0;
		}
		perc_frac = min(perc/awc[s-1], wcont[s-1]);

		wcont[s-1] -= perc_frac;
		wcont[s] += perc_frac * awc[s-1] / awc[s];
		if (wcont[s] > 1) {      // for the paper1, here we changed to topo, instead of 1. !!!!!!!
			runoff_drain += (wcont[s]-1)*awc[s];
			wcont[s] = 1;
		}

		// Deduct AET from this soil layer
		// BLARP! Quick fix here to prevent negative soil water

		wcont[s] -= aet_layer[s] / awc[s];
		if (wcont[s] < 0.0) {
			wcont[s] = 0.0;
		}
	}

	// Baseflow runoff (Dieter Gerten 021216) (rain or snowmelt days only)
	double runoff_baseflow = 0.0;
	if (percolate) {
		double perc_baseflow=BASEFLOW_FRAC*perc_base*pow(wcont[NSOILLAYER-1],perc_exp);
		// guess2008 - Added "&& rain_melt >= runoff_surf" to guarantee nonnegative baseflow.
		if (perc_baseflow > rain_melt - runoff_surf && rain_melt >= runoff_surf) {
			perc_baseflow = rain_melt - runoff_surf;
		}

		// Deduct from water content of bottom soil layer

		perc_frac = min(perc_baseflow/awc[NSOILLAYER-1], wcont[NSOILLAYER-1]);
		wcont[NSOILLAYER-1] -= perc_frac;
		runoff_baseflow = perc_frac * awc[NSOILLAYER-1];
	}
	
	runoff = runoff_surf + runoff_drain + runoff_baseflow;
	//when there is surface runoff, the patch.stand.gridcell.surf_perco will be greater than 0; when there is no runoff. there will be water percolate into the first layer,
		// so the patch.stand.gridcell.surf_perco will be negative. the value of surf_perco will be to always 0 when DA equals to 0
   double perc_cap=0.0;
	patch.stand.gridcell.surf_perco=0.0;
	if(runoff_surf>0)
		patch.stand.gridcell.surf_perco=runoff_surf;
	//else if (patch.stand.gridcell.DA==0)
		//patch.stand.gridcell.surf_perco=0;
	else{
		//perc_cap = perc_base*pow(wcont[0],perc_exp);    //in millimetres    
	    perc_cap=(1-wcont[0])*awc[0];    //calculate the unsaturated part of upper soil layer
		patch.stand.gridcell.surf_perco=-perc_cap;      
	}
	patch.arunoff += runoff;
	runoff_base_1=runoff_baseflow;
	runoff_drain_1=runoff_drain;
	runoff_surf_1=runoff_surf;

	//patch.stand.gridcell.surf_runoff+=runoff_surf;
	patch.drunoff[date.day] = runoff;
	patch.abaserunoff += runoff_base_1;    
	patch.adrainrunoff += runoff_drain_1;
	patch.asurfrunoff += runoff_surf_1;
	
	
	//testing ,surface runoff daily output
	//patch.dsurf_runoff[date.year][date.day]=runoff_surf_1;
	//if (date.year==300 && date.day==167&&patch.stand.gridcell.lon==19.004&&patch.stand.gridcell.lat==68.3215)
		//printf("check it here");
	//check the water balance for each gridcell(vertical), since we just have one patch at this moment.

	//patch.dwbal_vert=rain_melt_prev+patch.dadd_water-patch.evap-aet_layer[0]-aet_layer[1]-runoff-(wcont[0]-wcont_prev[0])*awc[0]-(wcont_evap-wcont_evap_prev)*awc[0]-(wcont[1]-wcont_prev[1])*awc[1];    // the values of patch.wbal_vert depict the values of differences from real percolation and percolation capability.
	
	//patch.wbal_vert[date.day]=rain_melt_prev+patch.dadd_water-patch.evap-aet_layer[0]-aet_layer[1]-runoff-(wcont[0]-wcont_prev[0])*awc[0]-(wcont[1]-wcont_prev[1])*awc[1];
	//if(wbal_vert!=0)
		//dprintf("\nwater balance failed");

	patch.aaet += patch.aet_tot;
	patch.aevap += patch.evap;
	//patch.m_wbal_vert[date.month]+=patch.dwbal_vert;
	//
	patch.maet[date.month] += patch.aet_tot;
	patch.mevap[date.month] += patch.evap;
	patch.mrunoff[date.month] += runoff;
	patch.msurf_add[date.month] += patch.stand.gridcell.dsurf_ave;     //jing add it. this is used to store the lateral water transfer every month.
	 

	patch.msurfrunoff[date.month] += runoff_surf;
	patch.mdrainrunoff[date.month]+= runoff_drain;
	patch.mbaserunoff[date.month] += runoff_baseflow;

	//patch.mprec[date.month] += rain_melt;




	// guess2008 - DLE - update awcont
	// Original algorithm by Thomas Hickler
	for (s=0; s<NSOILLAYER; s++) {

		// Reset the awcont array on the first day of every year
		if (date.day == 0) {
			awcont[s] = 0.0;
			if (s == 0) {
				patch.growingseasondays = 0;
			}
		}

		// If it's warm enough for growth, update awcont with this day's wcont
		if (climate.temp > 5.0) {
			awcont[s] += wcont[s];
			if (s==0) {
				patch.growingseasondays++;
			}
		}

		// Do the averaging on the last day of every year
		if (date.islastday && date.islastmonth) {
			awcont[s] /= (double)patch.growingseasondays;
		}
		// In case it's never warm enough:
		if (patch.growingseasondays < 1) {
			awcont[s] = 1.0;
		}
	}
}

/// Derive and re-distribute available rain-melt for today
/** Function to be called after interception and before canopy_exchange
 *  Calculate snowmelt
 *  If there's any rain-melt available, refill top layer, leaving any excessive
 *  rainmelt to be re-distributed later in hydrology_lpjf
 */
void initial_infiltration(Patch& patch, Climate& climate) {

	Soil& soil = patch.soil;
	patch.dprec = climate.prec;    //jing add it
	soil.wcont_prev[0]=soil.wcont[0];   //jing add it
	soil.wcont_evap_prev=soil.wcont_evap;//jing add it
	snow(climate.prec - patch.intercep, climate.temp, soil.snowpack, soil.rain_melt);
	soil.rain_melt_prev=soil.rain_melt;    
	
	//jing add it.
	if(date.year==0 && date.day==0)
		patch.dadd_water=0.0;
	else 
		patch.dadd_water=patch.stand.gridcell.dsurf_ave;   //jing add it. it always greater or equal to 0
	//soil.percolate = soil.rain_melt >= 0.1;   // standard LPJ
	


	soil.percolate = (soil.rain_melt+patch.dadd_water)>0.1;// jing change it
	soil.max_rain_melt = soil.rain_melt+patch.dadd_water;
    if (soil.percolate) {
		soil.wcont[0] += (soil.rain_melt+patch.dadd_water) / soil.soiltype.awc[0];

		if (soil.wcont[0] > 1) {
			soil.rain_melt = (soil.wcont[0] - 1) * soil.soiltype.awc[0];
			soil.wcont[0] = 1;
		} else {
			soil.rain_melt = 0;			
		}
		soil.wcont_evap = soil.wcont[0];
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SOIL WATER
// Call this function each simulation day for each modelled area or patch, following
// calculation of vegetation production and evapotranspiration and before soil organic
// matter and litter dynamics

void soilwater(Patch& patch, Climate& climate) {

	// DESCRIPTION
	// Performs daily accounting of soil water

	// Sum vegetation phenology-weighted FPC
	// Fraction of grid cell subject to evaporation from soil surface is
	// complement of summed vegetation projective cover (FPC)
	double fpc_phen_total = 0.0;	// phenology-weighted FPC sum for patch

	Vegetation& vegetation = patch.vegetation;
	vegetation.firstobj();
	while (vegetation.isobj) {
		Individual& indiv = vegetation.getobj();

		fpc_phen_total += indiv.fpc * indiv.phen;

		vegetation.nextobj();
	}

	Soil& soil = patch.soil;

	hydrology_lpjf(patch, climate, soil.rain_melt, soil.wcont_prev, soil.rain_melt_prev, soil.wcont_evap_prev, soil.soiltype.perc_base, soil.soiltype.perc_exp, soil.soiltype.awc, max(1.0-fpc_phen_total,0.0),
			soil.snowpack, soil.percolate, soil.max_rain_melt, soil.awcont, soil.wcont,
			soil.wcont_evap, soil.runoff, soil.runoff_surf_1, soil.runoff_drain_1,soil.runoff_base_1);
}

///////////////////////////////////////////////////////////////////////////////////////
// REFERENCES
//
// Haxeltine A & Prentice IC 1996 BIOME3: an equilibrium terrestrial biosphere
//   model based on ecophysiological constraints, resource availability, and
//   competition among plant functional types. Global Biogeochemical Cycles 10:
//   693-709
//
// Bondeau, A., Smith, P.C., Zaehle, S., Schaphoff, S., Lucht, W., Cramer, W.,
//   Gerten, D., Lotze-Campen, H., M�ller, C., Reichstein, M. and Smith, B. (2007),
//   Modelling the role of agriculture for the 20th century global terrestrial carbon balance.
//   Global Change Biology, 13: 679�706. doi: 10.1111/j.1365-2486.2006.01305.x
//
// Rost, S., D. Gerten, A. Bondeau, W. Luncht, J. Rohwer, and S. Schaphoff (2008),
//   Agricultural green and blue water consumption and its influence on the global
//   water system, Water Resour. Res., 44, W09405, doi:10.1029/2007WR006331
